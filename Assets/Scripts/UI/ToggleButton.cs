﻿using UnityEngine;
using UniRx;
using UnityEngine.UI;
using System;

namespace Game.Scene.UI
{
  public class ToggleButton : MonoBehaviour
  {
#pragma warning disable 649
    [SerializeField] private Button button;
    [SerializeField] private MaskableGraphic bg;
    [SerializeField] private GameObject startText;
    [SerializeField] private GameObject stopText;
    [SerializeField] private Color enableBgColor;
    [SerializeField] private Color disableBgColor;
    [SerializeField] private bool isEnabled;
#pragma warning restore 649

    public Func<bool, bool> OnClick { get; set; }

    private void Awake()
    {
      SwitchImages();
      button.OnClickAsObservable().Subscribe(_ =>
      {
        bool clickResult = OnClick.Invoke(!isEnabled);
        if (clickResult == true)
        {
          isEnabled = !isEnabled;
          SwitchImages();
        }
      }).AddTo(this);
    }

    private void SwitchImages()
    {
      startText.SetActive(!isEnabled);
      stopText.SetActive(isEnabled);
      bg.color = isEnabled ? enableBgColor : disableBgColor;
    }
  }
}
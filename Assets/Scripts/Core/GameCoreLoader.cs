﻿using UnityEngine;
using UniRx;

namespace Game.Core
{
  public class GameCoreLoader : MonoBehaviour
  {
    private static GameCore _gameCore;

    private void Awake()
    {
      if (_gameCore == null)
      {
        Input.multiTouchEnabled = false;
        Application.targetFrameRate = 60;
        _gameCore = new GameCore();
        DontDestroyOnLoad(this.gameObject);
        _gameCore.AddTo(this);
      }
      else
      {
        Destroy(gameObject);
      }
    }
  }
}
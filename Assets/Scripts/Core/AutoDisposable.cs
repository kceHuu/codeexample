﻿using System;
using System.Collections.Generic;

namespace Game
{
  public abstract class AutoDisposable : IDisposable
  {
    private List<IDisposable> _disposableObjects;
    private bool _alreadyDisposed;

    public void Dispose()
    {
      if (_alreadyDisposed)
      {
        return;
      }
      _alreadyDisposed = true;

      if (_disposableObjects != null)
      {
        List<IDisposable> objects = _disposableObjects;
        for (int i = objects.Count - 1; i >= 0; i--)
        {
          objects[i]?.Dispose();
        }
        objects.Clear();
      }

      OnDispose();
    }

    public virtual void OnDispose() { }

    public T AddDisposable<T>(T disposable) where T : IDisposable
    {
      if (_alreadyDisposed)
      {
        return default;
      }

      if (_disposableObjects == null)
      {
        _disposableObjects = new List<IDisposable>(1);
      }
      _disposableObjects.Add(disposable);
      return disposable;
    }
  }
}

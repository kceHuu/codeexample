﻿using System;
using Game.Managers;
using Game.Scene;
using Game.Scene.Config;

namespace Game.Core
{
  public class GameCore : AutoDisposable
  {
    private readonly ContentManager _contentManager;
    private readonly ProfileManager _profileManager;
    private readonly TextManager _textManager;
    private readonly UserProfile _userProfile;

    private readonly GamePlayModel _gamePlayModel;

    public GameCore()
    {
      _contentManager = new ContentManager(new ContentManager.Ctx
      {
        contentPath = "ContentConfig",
      });
      AddDisposable(_contentManager);

      _textManager = new TextManager(new TextManager.Ctx
      {
        contentManager = _contentManager,
      });
      AddDisposable(_textManager);

      _profileManager = new ProfileManager();
      AddDisposable(_profileManager);

      _userProfile = _profileManager.Profile;

      _gamePlayModel = new GamePlayModel(new GamePlayModel.Ctx
      {
        contentManager = _contentManager,
        textManager = _textManager,
      });
      AddDisposable(_gamePlayModel);
    }
  }
}
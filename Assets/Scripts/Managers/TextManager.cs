﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using UnityEngine;

namespace Game.Managers
{
  public class TextManager : AutoDisposable, ITextManager
  {
    public struct Ctx
    {
      public IContentManager contentManager;
    }

    private readonly Dictionary<string, string> _strings;

    public TextManager(Ctx ctx)
    {
      _strings = new Dictionary<string, string>();
      SystemLanguage systemLanguage = Application.systemLanguage;
      string locale = systemLanguage == SystemLanguage.Russian ? "ru" : "en";
      TextAsset textAsset = ctx.contentManager.LoadText($"strings_{locale}");
      FillDictionary(textAsset.text);
    }

    public string GetText(string key)
    {
      if (string.IsNullOrEmpty(key))
        return string.Empty;
      MatchCollection clearKeys = Regex.Matches(key, @"\{([^}]*)\}");
      if (clearKeys.Count > 0)
      {
        key = clearKeys[0].Groups[1].Value;
      }
      if (_strings.TryGetValue(key, out string resultString))
      {
        return resultString;
      }
      return string.Empty;
    }

    public string GetText<T>(string key, T arg)
    {
      return GetText(key).Replace("#", arg.ToString());
    }

    public string GetText<T>(string key, params T[] args)
    {
      string result = GetText(key);
      for (int i = 0; i < args.Length; i++)
      {
        result = result.Replace($"[{i.ToString()}]", args[i].ToString());
      }
      return result;
    }

    private void FillDictionary(string xmlText)
    {
      XmlDocument xDoc = new XmlDocument();
      xDoc.LoadXml(xmlText);
      if (xDoc.DocumentElement == null)
      {
        return;
      }
      foreach (XmlNode node in xDoc.DocumentElement)
      {
        if (node.Name != "string")
        {
          continue;
        }
        if (node.Attributes == null || node.Attributes.Count < 2)
        {
          continue;
        }
        string key = node.Attributes[0].Value;
        string value = node.Attributes[1].Value;
        _strings[key] = value;
      }
    }

    public override void OnDispose()
    {
      _strings.Clear();
    }
  }
}
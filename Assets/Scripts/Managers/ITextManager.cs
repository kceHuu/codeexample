﻿namespace Game.Managers
{
  public interface ITextManager
  {
    string GetText(string key);
    string GetText<T>(string key, T arg);
    string GetText<T>(string key, params T[] args);
  }
}
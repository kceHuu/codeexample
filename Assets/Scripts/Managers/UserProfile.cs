﻿using UnityEngine;

namespace Game.Managers
{
  [System.Serializable]
  public class UserProfile
  {
    [SerializeField]
    public XmlSerializableDictionary<string, int> propertyDictionary = new XmlSerializableDictionary<string, int>();

    public int GetProperty(string key, int defaultValue = 0)
    {
      if (propertyDictionary.TryGetValue(key, out int value))
      {
        return value;
      }
      return defaultValue;
    }

    public void SetProperty(string key, int value)
    {
      propertyDictionary[key] = value;
    }

    public void RemoveAllProperties()
    {
      propertyDictionary.Clear();
    }
  }
}
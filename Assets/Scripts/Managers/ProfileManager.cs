﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace Game.Managers
{
  public class ProfileManager : AutoDisposable
  {
    public UserProfile Profile { get; private set; }

    private readonly string _filePath;
    private const string FILENAME = "profile.dat";
    private const string VI_KEY = "4kffrdp5cxotx19bd73mf056vaqwb930";

    public ProfileManager()
    {
      _filePath = System.IO.Path.Combine(Application.persistentDataPath, "game_data");
      Directory.CreateDirectory(_filePath);
      LoadProfile();
    }

    private void LoadProfile()
    {
      string path = System.IO.Path.Combine(_filePath, FILENAME);
      if (File.Exists(path) == false)
      {
        Profile = new UserProfile();
        return;
      }

      string encryptString = string.Empty;
      using (StreamReader sr = new StreamReader(path))
      {
        encryptString = sr.ReadToEnd();
      }
      string decryptString = Decrypt(encryptString);
      StringReader strStream = new StringReader(decryptString);
      XmlSerializer xml = new XmlSerializer(typeof(UserProfile));
      Profile = xml.Deserialize(strStream) as UserProfile;
    }

    public void SaveProfile()
    {
      if (Profile == null)
      {
        return;
      }
      XmlSerializer xml = new XmlSerializer(typeof(UserProfile));
      StringWriter strStream = new StringWriter();
      xml.Serialize(strStream, Profile);
      string xmlString = strStream.ToString();
      string encryptString = Encrypt(xmlString);

      string path = System.IO.Path.Combine(_filePath, FILENAME);
      using (StreamWriter outputFile = new StreamWriter(path))
      {
        outputFile.WriteLine(encryptString);
      }
    }

    private string Encrypt(string toEncrypt)
    {
      byte[] keyArray = Encoding.UTF8.GetBytes(VI_KEY);
      byte[] toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);
      RijndaelManaged rDel = new RijndaelManaged();
      rDel.Key = keyArray;
      rDel.Mode = CipherMode.ECB;
      rDel.Padding = PaddingMode.PKCS7;
      ICryptoTransform cTransform = rDel.CreateEncryptor();
      byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
      return System.Convert.ToBase64String(resultArray, 0, resultArray.Length);
    }

    private string Decrypt(string toDecrypt)
    {
      byte[] keyArray = Encoding.UTF8.GetBytes(VI_KEY);
      byte[] toEncryptArray = System.Convert.FromBase64String(toDecrypt);
      RijndaelManaged rDel = new RijndaelManaged
      {
        Key = keyArray,
        Mode = CipherMode.ECB,
        Padding = PaddingMode.PKCS7
      };
      ICryptoTransform cTransform = rDel.CreateDecryptor();
      byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
      return Encoding.UTF8.GetString(resultArray);
    }

    public override void OnDispose()
    {
      //_profile.RemoveAllProperties();
    }
  }
}
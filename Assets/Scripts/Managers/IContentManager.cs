﻿using System.Collections.Generic;
using Game.Scene;
using UnityEngine;
using MaterialType = Game.Scene.ObjectOutline.MaterialType;

namespace Game.Managers
{
  public interface IContentManager
  {
    SceneObject LoadObjectPrefab(string prefabName);
    GameObject LoadPrefab(string prefabName);
    Sprite LoadSprite(string spriteName);
    TextAsset LoadText(string assetName);
    Material LoadMaterial(MaterialType materialType);
    List<string> AllObjectsNames { get; }
  }
}
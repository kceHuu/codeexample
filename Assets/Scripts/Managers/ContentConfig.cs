﻿using Game.Scene;
using UnityEngine;

namespace Game.Managers
{
  [CreateAssetMenu(fileName = "ContentConfig.asset", menuName = "ContentConfig")]
  internal class ContentConfig : ScriptableObject
  {
    public SceneObject[] objects;
    public Sprite[] sprites;
    public TextAsset[] texts;
    public GameObject[] uiPrefabs;
    public GameObject[] otherPrefabs;
    public Material defaultMaterial;
    public Material normalOutlineMaterial;
    public Material dangerOutlineMaterial;
    public Material greenOutlineMaterial;
    public Material untouchableMaterial;
  }
}

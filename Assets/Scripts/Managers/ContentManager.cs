﻿using System.Collections.Generic;
using Game.Scene;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Game.Managers
{
  public class ContentManager : AutoDisposable, IContentManager
  {
    public struct Ctx
    {
      public string contentPath;
    }

    private readonly ContentConfig _config;
    private readonly Dictionary<string, SceneObject> _objects;
    private readonly Dictionary<string, GameObject> _otherPrefabs;
    private readonly Dictionary<string, Sprite> _sprites;
    private readonly Dictionary<string, TextAsset> _texts;

    public ContentManager(Ctx ctx)
    {
      _config = Resources.Load<ContentConfig>(ctx.contentPath);
      _objects = new Dictionary<string, SceneObject>();
      _otherPrefabs = new Dictionary<string, GameObject>();
      _sprites = new Dictionary<string, Sprite>();
      _texts = new Dictionary<string, TextAsset>();
      FillDictionary(_objects, _config.objects);
      FillDictionary(_otherPrefabs, _config.uiPrefabs);
      FillDictionary(_otherPrefabs, _config.otherPrefabs);
      FillDictionary(_sprites, _config.sprites);
      FillDictionary(_texts, _config.texts);
    }

    public SceneObject LoadObjectPrefab(string prefabName)
    {
      _objects.TryGetValue(prefabName, out SceneObject result);
      return result;
    }

    public GameObject LoadPrefab(string prefabName)
    {
      _otherPrefabs.TryGetValue(prefabName, out GameObject result);
      return result;
    }

    public Sprite LoadSprite(string spriteName)
    {
      _sprites.TryGetValue(spriteName, out Sprite result);
      return result;
    }

    public TextAsset LoadText(string assetName)
    {
      _texts.TryGetValue(assetName, out TextAsset result);
      return result;
    }

    public Material LoadMaterial(ObjectOutline.MaterialType materialType)
    {
      switch (materialType)
      {
        case ObjectOutline.MaterialType.DEFAULT:
          return _config.defaultMaterial;
        case ObjectOutline.MaterialType.NORMAL_OUTLINE:
          return _config.normalOutlineMaterial;
        case ObjectOutline.MaterialType.DANGER_OUTLINE:
          return _config.dangerOutlineMaterial;
        case ObjectOutline.MaterialType.GREEN_OUTLINE:
          return _config.greenOutlineMaterial;
        case ObjectOutline.MaterialType.UNTOUCHABLE_OUTLINE:
          return _config.untouchableMaterial;
      }
      return null;
    }

    public List<string> AllObjectsNames
      => new List<string>(_objects.Keys);

    private void FillDictionary<T>(IDictionary<string, T> dict, IReadOnlyList<T> gameObjects) where T : Object
    {
      if (gameObjects == null || gameObjects.Count <= 0)
      {
        return;
      }
      for (int i = 0; i < gameObjects.Count; i++)
      {
        T gameObj = gameObjects[i];
        if (gameObj != null)
        {
          if (dict.ContainsKey(gameObj.name))
          {
            Debug.LogWarning($"duplicating '{gameObj.name}' in content");
          }
          dict[gameObj.name] = gameObj;
        }
      }
    }

    public override void OnDispose()
    {
      _objects.Clear();
      _otherPrefabs.Clear();
      _sprites.Clear();
      _texts.Clear();
      Resources.UnloadAsset(_config);
    }
  }
}